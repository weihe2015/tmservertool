'use strict'

//Call this function when the page loads (the "ready" event)
$(document).ready(function() {

	
	// Initialize TM List dropdown
	$('#TM_Option').selectpicker();
	
	// Page is first loaded.
	if(sessionStorage.getItem("tmList") === null)
	{
		beginAjax("Initial loading, please wait...");
		
		$("#advancedBtn_label").attr("style", "float: left; color:#ccc;");
		
		$.ajax({
			type: 'POST',
			url: '/Tool/TmList',
			contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
			success: function(data){
				var jsonData = jQuery.parseJSON(data);
	
				// Store TM List data in session storage cache
				sessionStorage.setItem("tmList",data);
				
				$('#gif').css('display','none');
				$('.overlay').hide();
				$("input").prop('disabled', false);
				$("button").prop('disabled', false);

				$("#loading-text").empty();
				
				$.each(jsonData, function(key, value){
					$('#TM_Option')
			         .append($("<option></option>")
			                    .attr("value",value).attr("selected",true)
			                    .text(value)); 
				});
				var option_html = $('#TM_Option')[0];

				$('.option-div').find('select:first').remove();
				$('.option-div').append(option_html);
				$('.option-div').css("visibility", 'visible');
				
				$('#TM_Option').selectpicker();
				$('.option-div').find('div:first').remove();
				
				$('.select-All-div').css('visibility','visible');
				$("input").prop('disabled', false);
				
				$("#advancedBtn_label").attr("style", "color:#000000");
			},
			error: function(xhr, exception){
				var msg = '';
				if (xhr.status === 0) {
					msg = 'Not connect.\n Verify Network.';
				} else if (xhr.status == 404) {
					msg = 'Requested page not found. [404]';
				} else if (xhr.status == 500) {
					msg = 'Internal Server Error [500].';
				} else if (exception === 'timeout') {
					msg = 'Time out error.';
				} else if (exception === 'abort') {
					msg = 'Ajax request aborted.';
				} else {
					msg = 'Uncaught Error.\n' + xhr.responseText;
				}
				console.log("Sorry, error happen: " + msg);
			}
		});
	}
	else
	{
		// Load TM List from session storage cache
		var data = sessionStorage.getItem("tmList");
		var jsonData = jQuery.parseJSON(data);
		$.each(jsonData, function(key, value){
			$('#TM_Option')
	         .append($("<option></option>")
	                    .attr("value",value).attr("selected",true)
	                    .text(value)); 
		});
		var option_html = $('#TM_Option')[0];

		$('.option-div').find('select:first').remove();
		$('.option-div').append(option_html);
		$('.option-div').css("visibility", 'visible');
		$('#TM_Option').selectpicker();
		$('.option-div').find('div:first').remove();
		
		$('.select-All-div').css('visibility','visible');
		
	}
	
	// Function to handle display advanced search option button.
	$('#Advanced_Options_ID').change(function(){
		
		// Handle get TM List button
		if($('.get-TM-btn-div').css('display') == 'none')
		{
			$('.get-TM-btn-div').show('slow');
			$('.Advanced_Option_div').hide('slow');
			
			$('.Advanced_Option_div').addClass("click_Advanced_Option");
		}
		else
		{
			$('.get-TM-btn-div').hide('slow');
		}
		
		// Handle all advanced Search options fieldset
		if($('#advancedOptions').css('display') == 'none')
		{
			$('#advancedOptions').show('slow');
		}
		else
		{
			$('#advancedOptions').hide('slow');
		}
		
	});
	
	// Function to handle advance search options.
	$('.close_fieldset').click(function(evt){
		evt.preventDefault();
		$('#advancedOptions').hide('slow');
		$('.Advanced_Option_div').show('slow');
		$('.Advanced_Option_div').removeClass("click_Advanced_Option");
		$('.get-TM-btn-div').hide('slow');
		$('#Advanced_Options_ID').prop("checked", false);
		
		// Set advanced options back to default values.
		$('#max_result_ID').val(50);
		$('#concordance_form_search_location').val('SOURCE_AND_TARGET');
		$('.checkbox_opt').prop("checked", false);
		
	});

	// select all search TM button
	$('.select-All-Btn').click(function(evt){
		var options = valuesOf($('#TM_Option').find('option'));
		if($('#TM_Option').hasClass('allOptionIsSelected'))
		{
			$('#TM_Option').selectpicker('deselectAll');
			$('#TM_Option').removeClass('allOptionIsSelected');
			$(this).prop('checked', true);
		}
		else
		{
			$('#TM_Option').selectpicker('selectAll');
			$('#TM_Option').addClass('allOptionIsSelected');
			$(this).prop('checked', false);
		}
	});
		
	$('#searchForm').submit(function(evt){
		
		beginAjax("Searching, please wait...");
		
		$(".get-TM-btn-div").hide('slow');
		$("#advancedOptions").hide('slow');
		$("#search-result").children().remove();
		$("#advancedBtn_label").attr("style", "float: left; color:#ccc;");
		
		var formData = {
			'concordance_search_string' : $('textarea[name=concordance_search_string]').val(),
			'concordance_max_result' : $('input[name=concordance_max_result]').val(),
			'concordance_case_sensitive' : $('input[name=concordance_case_sensitive]:checked').val(),
			'concordance_match_whole_word' : $('input[name="concordance_match_whole_word"]:checked').val(),
			'concordance_search_location' : $('select[name=concordance_search_location]').val(),
			'concordance_search_TM_List' : JSON.stringify($('select[name=TM_Option_select]').val())
		}
		
		$.ajax({
			type: 'POST',
			url: '/Tool/ConcordanceSearch',
			contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
			data: 'data='+JSON.stringify(formData),
			success: function(data){
				$('#gif').css('display','none');
				$('#Concordance-Search-div').css('display','block');
				$('#search-result').append(data);
				
				//$('#result-table').DataTable();
			    $('#result-table').DataTable({
			        'scrollX': true,
			        "autoWidth": true,
			        bFilter: false,
			        aLengthMenu: [
			            [10, 25, 50, 100, -1],
			            [10, 25, 50, 100, "All"]
			        ],
			        iDisplayLength: 10,
			        ordering: false,

			        searching: true,
			        initComplete: function () {
			            this.api().columns([1,3]).every( function () {
			                var column = this;
			                var defaultOption = "";
			                if(column[0][0] == 1)
			                {
			                	defaultOption = "All Source Languages";
			                }
			                else if(column[0][0] == 3)
			                {
			                	defaultOption = "All Target Languages";
			                }
			                var select = $('<select><option value="">'+defaultOption+'</option></select>')
			                    .appendTo( $(column.header()).empty() )
			                    .on( 'change', function () {
			                        var val = $.fn.dataTable.util.escapeRegex(
			                            $(this).val()
			                        );
			                        column
			                            .search( val ? '^'+val+'$' : '', true, false )
			                            .draw();
			                    } );
			                column.data().unique().sort().each( function ( d, j ) {
			                    select.append( '<option value="'+d+'">'+d+'</option>' )
			                } );
			            } );
			        }
			    });
				
			    endAjax();
			    
				$("#advancedBtn_label").attr("style", "color:#000000");
				
				if($('.Advanced_Option_div').hasClass('click_Advanced_Option'))
				{
					$('#advancedOptions').show('slow');
					if($('.get-TM-btn').hasClass('Click_TM_Btn'))
					{
						$('.option-div').css("visibility", 'visible');
					}
				}
				else
				{
					$('#advancedOptions').hide('slow');
				}
				$('html, body').stop().animate({
                    scrollTop: $("#search-result").offset().top+20
                }, 1000);
			},
			error: function(xhr, exception){
				var msg = '';
				if (xhr.status === 0) {
					msg = 'Not connect.\n Verify Network.';
				} else if (xhr.status == 404) {
					msg = 'Requested page not found. [404]';
				} else if (xhr.status == 500) {
					msg = 'Internal Server Error [500].';
				} else if (exception === 'timeout') {
					msg = 'Time out error.';
				} else if (exception === 'abort') {
					msg = 'Ajax request aborted.';
				} else {
					msg = 'Uncaught Error.\n' + xhr.responseText;
				}
				console.log("Sorry, error happen: " + msg);
			}
		});
		event.preventDefault();
	});

	
}); // End of document


function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
    {
    	return false;
    }  
    return true;
}

function valuesOf(elements) {
    return $.map(elements, function(element) {
        return element.value;
    });
}

window.exportExcel = function exportExcel(table) {

	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();
	
	var hh = today.getHours();
	var minutes = today.getMinutes();
	var ss = today.getSeconds();
	
	if(dd<10) {
		dd='0'+dd
	} 

	if(mm<10) {
		mm='0'+mm
	} 

	today = mm+'/'+dd+'/'+yyyy;

	alasql('SELECT * INTO XLSX("'+today+'-search_result.xlsx",{headers:true}) FROM HTML("#'+table+'",{headers:true})');
}

function toggleSelectAll(control) {
    var allOptionIsSelected = (control.val() || []).indexOf("All") > -1;
    function valuesOf(elements) {
        return $.map(elements, function(element) {
            return element.value;
        });
    }

    if (control.data('allOptionIsSelected') != allOptionIsSelected) {
        // User clicked 'All' option
        if (allOptionIsSelected) {
            // Can't use .selectpicker('selectAll') because multiple "change" events will be triggered
            control.selectpicker('val', valuesOf(control.find('option')));
        } else {
            control.selectpicker('val', []);
        }
    } else {
        // User clicked other option
        if (allOptionIsSelected && control.val().length != control.find('option').length) {
            // All options were selected, user deselected one option
            // => unselect 'All' option
            control.selectpicker('val', valuesOf(control.find('option:selected[value!=All]')));
            allOptionIsSelected = false;
        } else if (!allOptionIsSelected && control.val().length == control.find('option').length - 1) {
            // Not all options were selected, user selected all options except 'All' option
            // => select 'All' option too
            control.selectpicker('val', valuesOf(control.find('option')));
            allOptionIsSelected = true;
        }
    }
    control.data('allOptionIsSelected', allOptionIsSelected);
}
