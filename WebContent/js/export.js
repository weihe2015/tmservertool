'use strict'

//Call this function when the page loads (the "ready" event)
$(document).ready(function() {
	
	// If user does not login, redirect user to login page
	if(sessionStorage.getItem("loginStatus") == null)
	{
		window.location.replace("login.html");
		return;
	}
	var table;
	// Page is first loaded.
	if(sessionStorage.getItem("tmList") === null)
	{
		beginAjax("Initial loading, please wait...");
		$.ajax({
			type: 'POST',
			url: '/Tool/TmList',
			contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
			success: function(data){
				table = constructExporTmInfoTable(data);
				
				// Store TM List data in session storage cache
				sessionStorage.setItem("tmList",data);
				endAjax();
			},
			error: function(xhr, exception){
				var msg = '';
				if (xhr.status === 0) {
					msg = 'Not connect.\n Verify Network.';
				} else if (xhr.status == 404) {
					msg = 'Requested page not found. [404]';
				} else if (xhr.status == 500) {
					msg = 'Internal Server Error [500].';
				} else if (exception === 'timeout') {
					msg = 'Time out error.';
				} else if (exception === 'abort') {
					msg = 'Ajax request aborted.';
				} else {
					msg = 'Uncaught Error.\n' + xhr.responseText;
				}
				console.log("Sorry, error happen: " + msg);
			}
		});
	}
	else
	{
		// Load TM List from session storage cache
		var data = sessionStorage.getItem("tmList");
		table = constructExporTmInfoTable(data);
	}
	
	// Handle export TM button
	$('#submitBtn').click(function(evt){
		evt.preventDefault();
		var totalExportedTms = table.rows('.selected').data().length;
		if(totalExportedTms == 0)
		{
			BootstrapDialogBox('TM Export Info','Please select at least one Tm to export.');
		}
		else
		{
			// Hide the table		
			$('.exporTmInfo').fadeOut('slow');
			beginAjax("");
			
			var completedTms = 1;
			table.rows('.selected').every(function(){
				var thisRow = this;
				var d = thisRow.data();
				
				var exportedMessage = "Exporting TMs, working on " + completedTms + " of " + totalExportedTms 
								+ ". Please do not close the browser.";
				$("#loading-text").text(exportedMessage);
				var formData = {		
						'tmName' : d[0],
						'tmPath' : d[4],
						'exportType' : $('#TM_Export_Type').val(),
						'includeAttributes' : $('input[name=includeAttributes]:checked').val(),
						'includePrivateTus' : $('input[name=includePrivateTusCheckbox]:checked').val()
				};
				$.ajax({
					type: 'POST',
					url: '/Tool/Export',
					contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
					data: 'data='+JSON.stringify(formData),
					success: function(data){
						console.log(data);
						d[8] = "Exported";
						completedTms++;
						showCompletedExportMessage(completedTms, totalExportedTms);
						table.row(thisRow).data(d).draw();
					},
					error: function(xhr, exception){
						var msg = '';
						if (xhr.status === 0) {
							msg = 'Not connect.\n Verify Network.';
						} else if (xhr.status == 404) {
							msg = 'Requested page not found. [404]';
						} else if (xhr.status == 500) {
							msg = 'Internal Server Error [500].';
						} else if (exception === 'timeout') {
							msg = 'Time out error.';
						} else if (exception === 'abort') {
							msg = 'Ajax request aborted.';
						} else {
							msg = 'Uncaught Error.\n' + xhr.responseText;
						}
						console.log("Sorry, error happen: " + msg);
						completedTms++;
						d[8] = "Failed";
						showCompletedExportMessage(completedTms, totalExportedTms);
						table.row(thisRow).data(d).draw();
					}
				});
			});	
			
		}
	});
	
});

function showCompletedExportMessage(completedTms, totalExportedTms)
{
	if(completedTms == totalExportedTms + 1)
	{
		endAjax();
		$('.exporTmInfo').fadeIn('slow');
		
		if(totalExportedTms == 1)
		{
			BootstrapDialogBox("TM Exported Info", 
				"Total " + totalExportedTms + " TM is exported to FTP. Please check the exported file on FTP.");
		}
		else 
		{
			BootstrapDialogBox("TM Exported Info", 
				"Total " + totalExportedTms + " TMs are exported to FTP. Please check the exported file on FTP.");
		}
	}
}

function constructExporTmInfoTable(data)
{
	var jsonData = jQuery.parseJSON(data);
	
	$.each(jsonData, function(key,value){
		var content = "<tr>";
		var count = 0;
		key.split("#").forEach(function(entry){
			// Add tmName class to pass it to servlet
			if(count == 0)
			{
				content += "<td class='tmNameInfo'>";
			}
			// Add hiddenInfo class to TM Path to pass it to servlet.
			else if(count == 4)
			{
				content += "<td class='tmPathInfo'>";
			}
			else
			{
				content += "<td>";
			}
			content += entry;
			content += "</td>";
			count++;
		});
		// Add ExportStatus class to Progress column in order to update its status.
		content += "<td class='exportStatus'>Pending</td></tr>";
		$('#exportTmTable tbody').append(content);
	});
	
	var table = $('#exportTmTable').DataTable({
		bFilter: false,
        aLengthMenu: [
            [10, 25, 50, 100, -1],
            [10, 25, 50, 100, "All"]
        ],
        iDisplayLength: 10,
        'scrollX': true,
        "autoWidth": false,
        dom: 'lBfrtip',
        ordering: false,
        searching: false,
        buttons: [
        {
        	text: '&nbsp;<button type="button" class="btn btn-outline-info btn-sm">Deselect All Rows</button>&nbsp;',
        	action: function(e, dt, node, config){
        		dt.rows('.selected').deselect();
				BootstrapDialogBox("TM Exported Info", 
						"All rows have been deselected.");
        	}
        }],
    	select: {
    		style: "multi"
    	},
    	language: {
            select: {
                rows: {
                	_: " %d rows selected",
                	0: " 0 row selected",
                	1: " 1 row selected"
                }
            }
        }
	});
	
	return table;
}



