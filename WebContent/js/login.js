'use strict'

//Call this function when the page loads (the "ready" event)
$(document).ready(function() {

	if(sessionStorage.getItem("loginStatus") == null)
	{
		$('#loginForm').submit(function(evt){
			evt.preventDefault();

			$(".login-subinfo").remove();
			$(".panel-login input[type='text']").removeClass("error");
			$(".panel-login input[type='password']").removeClass("error");
			
			$('#gif').css('display','block');
			$(".overlay").show();
			$("#loading-text").text("Log in, please wait...");
			
			$.ajax({
				type: 'POST',
				url: '/Tool/Login',
				contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
				data : $(this).serialize(),
				success: function(data){
					$('#gif').css('display','none');
					$('.overlay').hide();
					
					if(data.includes("true"))
					{
						sessionStorage.setItem("loginStatus","true");
						window.location.replace("search.html");
						
					}
					else if(data.includes("false, username"))
					{
						$(".login-info").append('<div class="login-subinfo" tabindex="5">UserName does not exist.</div>');
						$(".panel-login input[type='text']").addClass("error");
					}
					else
					{
						$(".login-info").append('<div class="login-subinfo" tabindex="5">Incorrect Password.</div>');
						$(".panel-login input[type='text']").addClass("error");
						$(".panel-login input[type='password']").addClass("error");
					}
				},
				error: function(xhr, exception){
					var msg = '';
					if (xhr.status === 0) {
						msg = 'Not connect.\n Verify Network.';
					} else if (xhr.status == 404) {
						msg = 'Requested page not found. [404]';
					} else if (xhr.status == 500) {
						msg = 'Internal Server Error [500].';
					} else if (exception === 'timeout') {
						msg = 'Time out error.';
					} else if (exception === 'abort') {
						msg = 'Ajax request aborted.';
					} else {
						msg = 'Uncaught Error. ' + xhr.responseText;
					}
					console.log("Sorry, error happen: " + msg);
				}
			});
			
		});
	}
	else
	{
		window.location.replace("search.html");
	}

	
});