'use strict'

//Call this function when the page loads (the "ready" event)
$(document).ready(function() {
	
	beginAjax("Initial loading, please wait...");
	
	TmNameAutoCompleteList();
	
	// check old Tm Name and remove item from result table if user types backspace key.
	$('#oldTmName').keyup(function(){
		var oldTmName = $(this).val();
	    var key = event.keyCode || event.charCode;
	    // User types backspace, delete key 46 does not work here.
	    if (key == 8)
	    {
	    	console.log(oldTmName);
	    	// no match in input box
	    	if (oldTmName.length == 0 && $('.result-tbody').children().length > 0)
	        {
				var table = $('#result-table').DataTable();
		        table.destroy();
		        $('.result-tbody').children().remove();
	        }
	    	// At least one match in input box.
	    	else if (oldTmName.length > 0)
	        {
	    		console.log($('.result-tbody').children().length);
	    		console.log(oldTmName);
	    		var validTmPaths = [];
	    		oldTmName.forEach(function(elem){
	    			validTmPaths.push(elem.split('#')[2]);
	    		});
	    		
	    		if (oldTmName.length < $('.result-tbody').children().length)
	    	    {
	    			var table = $('#result-table').DataTable();
	    			$('.result-tbody tr').each(function(){
	    				var tmPathInTable = $(':nth-child(4)', this).text();
	    				if (validTmPaths.indexOf(tmPathInTable) == -1)
	    			    {
	    					$(this).remove();
	    			    }
	    			});
	    	    }
	    		validTmPaths = [];
	        }
	    }
	});
	
	// check rename TM name
	$('#newTmName').focusout(function(){
		var name = $(this).val();
	
		if(!name)
		{
			$(this).addClass('error');
			if($(this).parent().parent().children('.error-message').length == 0)
			{
				$(this).parent().after('<div class="error-message" style="color:red">&#10033;The rename TM field is required.</div>');
			}
		}
		else if(name)
		{
			$(this).removeClass('error');
			$(this).parent().parent().children('.error-message').remove();
		}
		
		if(!checkTMName(name))
		{
			$(this).addClass('error');
			if($(this).parent().parent().children('.error-message').length == 0)
			{
				$(this).parent().after('<div class="error-message" style="color:red">&#10033;The rename TM name is not formatted. Only 0-9, A-Z, a-z, -, _ is allowed.</div>');
			}
		}
		else if(checkTMName(name))
		{
			$(this).removeClass('error');
			$(this).parent().parent().children('.error-message').remove();
		}
		
	});
	
	
	// Process the form with Rename button clicked
	$('#submitBtn').click(function(evt){
		evt.preventDefault();
		if($('#result-table tbody tr').find('td').length <= 1)
		{
			BootstrapDialogBox('TM Rename Action','Please select at least one TM to rename.');
		}
		else if($('#newTmName').val().length == 0)
		{
			BootstrapDialogBox('TM Rename Action','New TM Name cannot be empty.');
			if(!$('#newTmName').hasClass('error'))
			{
				$('#newTmName').addClass('error');
				if($('#newTmName').parent().parent().children('.error-message').length == 0)
				{
					$('#newTmName').parent().after('<div class="error-message" style="color:red">&#10033;The rename TM field is required.</div>');
				}
			}
				
		}
		else if($('#newTmName').hasClass('error'))
		{
			BootstrapDialogBox('TM Rename Action','Please correct the TM rename error before renaming.');
		}
		else
		{	
			var formData = [];
			$('#result-table tbody').find('tr').each(function(){
					var tmJson = {};
					tmJson.tmId = $(this).find('td:nth-child(1)').text();
					tmJson.oldTmName = $(this).find('td:nth-child(2)').text();
					tmJson.newTmName = $('#newTmName').val();
					formData.push(tmJson);
					
			});
			
			beginAjax("Processing renaming TM, please wait...");
			
			$.ajax({
				type: 'POST',
				url: '/Tool/Rename',
				contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
				data: 'data='+JSON.stringify(formData),
				success: function(data){			
					// Update successful
					// reset old tm Name
					$('#oldTmName').removeAttr('data-msdb-value');
					$('#oldTmName').val("");
					$('#newTmName').val("");
					
					// Reset dataTable
					var table = $('#result-table').DataTable();
			        table.destroy();
			        $('.result-tbody').children().remove();
			        $('.result').css('display','none');
			        
					// Remove the selected item in dropdown list
					$('.m-select-d-box__list-container').find('li').each(function(){
						
						if($(this).hasClass('m-select-d-box__list-item_selected'))
						{
							$(this).removeClass('m-select-d-box__list-item_selected');
						}
					});
					// get the latest tmInfoList and refresh dropdown list in search box
					TmNameAutoCompleteList();
					
					endAjax();
					
					BootstrapDialogBox('TM Rename Info','TM names have been updated successfully.');
				},
				error: function(xhr, exception){
					var msg = '';
					if (xhr.status === 0) {
						msg = 'Not connect.\n Verify Network.';
					} else if (xhr.status == 404) {
						msg = 'Requested page not found. [404]';
					} else if (xhr.status == 500) {
						msg = 'Internal Server Error [500].';
					} else if (exception === 'timeout') {
						msg = 'Time out error.';
					} else if (exception === 'abort') {
						msg = 'Ajax request aborted.';
					} else {
						msg = 'Uncaught Error.\n' + xhr.responseText;
					}
					console.log("Sorry, error happen: " + msg);
				}
			});
		}
	});
	
});

function checkTMName(name)
{
	var re = /^[0-9a-zA-Z\-_ ]+$/im;
	return re.test(name);
}

function TmNameAutoCompleteList()
{
	$.ajax({
		type: 'POST',
		url: '/Tool/TmInfo',
		contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
		success: function(data){
			
			var jsonData = jQuery.parseJSON(data);
			
			$('#oldTmName').mSelectDBox({
				"list": jsonData,
				"multiple": true,
				"autoComplete":  true,
				"onselect": function(msdbContext, event){
					$('.result').css('display','block');
					if($('.result-tbody').children().length > 0)
					{
						var table = $('#result-table').DataTable();
				        table.destroy();
				        $('.result-tbody').children().remove();
					}

					for(var obj in msdbContext._props.selectedcache)
					{
						var smObj = msdbContext._props.selectedcache[obj];
						var tmName = smObj.label;
						var tmpStr = smObj.value.split("#");
						var tmGroupName = tmpStr[0];
						var tmID = tmpStr[1];
						var tmPath = tmpStr[2];
						var tmSrcLanguage = tmpStr[3];
						var tmTgtLanguage = tmpStr[4];
						
						var columnStr = "<tr><td class=\"hiddenInfo\">"+tmID+"</td>";
						columnStr += "<td>"+tmName+"</td>";
						columnStr += "<td>"+tmGroupName+"</td>";
						columnStr += "<td>"+tmPath+"</td>";
						columnStr += "<td>"+tmSrcLanguage+"</td>";
						columnStr += "<td>"+tmTgtLanguage+"</td></tr>";
						
						$('.result-tbody').append(columnStr);
					}
					$('#result-table').DataTable({
						bFilter: false,
				        aLengthMenu: [
				            [10, 25, 50, 100, -1],
				            [10, 25, 50, 100, "All"]
				        ],
				        iDisplayLength: 10,
				        ordering: false,
				        searching: false
					});
				},
				
			});
			
			endAjax();
		},
		error: function(xhr, exception){
			var msg = '';
			if (xhr.status === 0) {
				msg = 'Not connect.\n Verify Network.';
			} else if (xhr.status == 404) {
				msg = 'Requested page not found. [404]';
			} else if (xhr.status == 500) {
				msg = 'Internal Server Error [500].';
			} else if (exception === 'timeout') {
				msg = 'Time out error.';
			} else if (exception === 'abort') {
				msg = 'Ajax request aborted.';
			} else {
				msg = 'Uncaught Error.\n' + xhr.responseText;
			}
			console.log("Sorry, error happen: " + msg);
		}
	});
}


