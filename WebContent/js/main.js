// Main javascript file for search.html, export.html, rename.html

'use strict'
//Call this function when the page loads (the "ready" event)
$(document).ready(function() {
	
	// If user does not login, redirect user to login page
	if(sessionStorage.getItem("loginStatus") == null)
	{
		window.location.replace("login.html");
		return;
	}
	
	// Sign out handler:
	$('.btn-signout').click(function(evt){
		evt.preventDefault();
		
	    BootstrapDialog.show({
	        title: 'SIGN OUT OPTIONS',
	        message: "You are signing out. All information will be lost",
	        buttons: [{
                label: 'Sign out',
                action: function(){
            		sessionStorage.removeItem("loginStatus");
            		window.onbeforeunload=function(){null}
            		window.location.replace("/Tool");
                }
            },{
	        	label: 'Cancel',
	            action: function(dialogItself){
	                dialogItself.close();
	            }
	        }]
	    });

	});
	
	$('.redirect').click(function(evt){
		window.onbeforeunload=function(){null}
	});
	
	// Pop out an message to confirm browser is exiting.
	window.onbeforeunload = function (e) {
		var message = "Your confirmation message goes here.",
		e = e || window.event;
		// For IE and Firefox
		if (e) {
			e.returnValue = message;
		}
		// For Safari
		return message;
	};
	
	// dynamic change the year of footer
	var today = new Date();
	var year = today.getFullYear();
	$('#footer').html('Copyright &copy; '+year+' Translations.com. All Rights Reserved.');
	
	
});

function beginAjax(message)
{
	$('#gif').css('display','block');
	$(".overlay").show();
	$("#loading-text").append(message);
	$("input").prop('disabled', true);
	$("button").prop('disabled', true);
}

function endAjax()
{
	$('#gif').css('display','none');
	$('.overlay').hide();
	$("input").prop('disabled', false);
	$("button").prop('disabled', false);
	$("#loading-text").empty();
	$("#loading-text").text("");
}


function BootstrapDialogBox(titleString, messageString)
{
    BootstrapDialog.show({
        title: titleString,
        message: messageString,
        buttons: [{
        	label: 'Close',
            action: function(dialogItself){
                dialogItself.close();
            }
        }]
    });
}