'use strict';

//Call this function when the page loads (the "ready" event)
$(document).ready(function() {
	
    $('#result-table').DataTable( {
        initComplete: function () {
            this.api().columns([2]).every( function () {
                var column = this;
                var select = $('<select><option value="">all</option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
    } );
})

window.exportCSV = function exportCSV() {
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();
	
	var hh = today.getHours();
	var minutes = today.getMinutes();
	var ss = today.getSeconds();
	
	if(dd<10) {
		dd='0'+dd
	} 

	if(mm<10) {
		mm='0'+mm
	} 

	today = mm+'/'+dd+'/'+yyyy;

	var data = alasql('SELECT * FROM HTML("#result-all",{headers:true})');
	alasql('SELECT * INTO CSV("'+today+'-search_result.csv",{headers:true,separator:","}) FROM ?', [data]);
}