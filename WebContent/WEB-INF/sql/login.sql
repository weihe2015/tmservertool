/* Create a database for storing TM Server Tool user info*/
CREATE DATABASE IF NOT EXISTS TMServerTool;
use TMServerTool;

/* Create a table called USERINFO */
CREATE TABLE IF NOT EXISTS USERINFO (
	USER_ID BIGINT(20) NOT NULL AUTO_INCREMENT,
	USER_NAME VARCHAR(255) NOT NULL,
	USER_PASSWORD VARCHAR(255) NOT NULL,
	PRIMARY KEY (USER_ID)
)
COLLATE='utf8mb4_unicode_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1;

/* Insert user login info to USERINFO for initial login */
INSERT INTO USERINFO (USER_NAME, USER_PASSWORD) VALUES ("test_admin", "password1!");

/* Grant this table access to pd4 user */
GRANT ALL PRIVILEGES ON TMServerTool.* TO 'pd4'@'localhost';