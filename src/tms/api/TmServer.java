package tms.api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import tms.model.TmExportOption;
import tms.model.TmListInfo;
import tms.model.TmSearchOption;
import tms.model.TmSearchResult;
import tms.model.TmServerInfo;
import tms.utils.FtpUtils;
import tms.utils.HelperUtils;

public class TmServer {
	
	private String currTime;
	private HelperUtils instance;
	
	public TmServer()
	{
		setTime();
		HelperUtils instance = HelperUtils.getInstance();
		setHelperUtils(instance);
	}
	
	private void setHelperUtils(HelperUtils instance)
	{
		this.instance = instance;
	}
	
	private void setTime()
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date currdate = new Date();
		String currTime = dateFormat.format(currdate);
		this.currTime = currTime;
	}
	
	/**
	 * Get response as string
	 * @throws IOException 
	 * */
	private String getResponse(HttpURLConnection con) throws IOException
	{
		StringBuilder sb = new StringBuilder();
		BufferedReader br = new BufferedReader(new InputStreamReader(
				(con.getInputStream())));

		String output;
		while ((output = br.readLine()) != null) {
			sb.append(output);
		}
		
		return sb.toString();
	}
	
	private HttpURLConnection connect(String url, boolean isExport) throws IOException
	{
		URL object=new URL(url);

		HttpURLConnection con = (HttpURLConnection) object.openConnection();
		con.setDoOutput(true);
		con.setDoInput(true);
		con.setRequestProperty("Content-Type", "application/json");
		if(!isExport)
		{
			con.setRequestProperty("Accept", "application/json");
		}
		con.setRequestMethod("POST");
		
		return con;
	}
	
	public String login(TmServerInfo tmServerInfo, String tmlocation) throws IOException, JSONException
	{
		String securityTicket = "";
		System.setProperty("jsse.enableSNIExtension", "false");

		HttpURLConnection con = connect(tmServerInfo.getURL(), false);
		
		JSONObject loginInfoSent = new JSONObject();
		loginInfoSent.put("username", tmServerInfo.getTmUserName());
		loginInfoSent.put("password", tmServerInfo.getTmPassword());
		loginInfoSent.put("tmLocation", tmlocation);

		OutputStream os = con.getOutputStream();
		os.write(loginInfoSent.toString().getBytes());
		os.flush();

		if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
			instance.LogMessage(this.currTime + " " + tmlocation + 
					" Failed in login in TmServer.java: HTTP error code : " + con.getResponseCode());
			return "";
			//throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
		}

		String loginResponse = getResponse(con);
		JSONObject loginInfoReceived  = new JSONObject(loginResponse);

		if(loginInfoReceived.has("securityTicket"))
		{
			securityTicket = (String) loginInfoReceived.get("securityTicket");
		}
		
		con.disconnect();
		return securityTicket;
	}
	
	public void logout(TmServerInfo tmServerInfo, String securityTicket) throws IOException, JSONException {

		HttpURLConnection con = connect(tmServerInfo.getLogoutURL(), false);
		
		JSONObject loginInfoSent   = new JSONObject();
		loginInfoSent.put("securityTicket", securityTicket);

		OutputStream os = con.getOutputStream();
		os.write(loginInfoSent.toString().getBytes());
		os.flush();
		
		if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
			con.disconnect();
			instance.LogMessage(this.currTime + " " + tmServerInfo.getLogoutURL() +
					" Failed in logout in TmServer.java: HTTP error code : " + con.getResponseCode());
			return;
		}

		con.disconnect();
	}
	
	public List<TmSearchResult> concordance(TmServerInfo tmServerInfo, String securityTicket, 
			TmSearchOption searchOption, TmListInfo tmInfo) throws Exception
	{
		List<TmSearchResult> result = new ArrayList<TmSearchResult>(); 

		HttpURLConnection con = connect(tmServerInfo.getConcordanceURL(), false);
		
		JSONObject searchInfoSent = new JSONObject();
		searchInfoSent.put("securityTicket", securityTicket);
		searchInfoSent.put("query", searchOption.getSearchString());
		searchInfoSent.put("caseSensitive", searchOption.getSearchCaseSentitive());
		searchInfoSent.put("matchWholeWords", searchOption.getSearchMatchWholeWord());
		searchInfoSent.put("maxResults", searchOption.getSearchMaxResult());
		searchInfoSent.put("location", searchOption.getSearchLocation());
		
		OutputStream os = con.getOutputStream();
		os.write(searchInfoSent.toString().getBytes());
		os.flush();
		
		if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
			instance.LogMessage(this.currTime + " " + tmInfo.getPath() + 
					" Failed is concordance Search in TmServer.java: HTTP error code : " + con.getResponseCode());
			return result;
			//throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
		}
		
		String concordanceResponse = getResponse(con);
		
		JSONObject searchResult = new JSONObject(concordanceResponse);
		
		int resultNum = searchResult.getInt("resultNum");
		if(resultNum == 0)
		{
			// No result is found.
			return null;
		}
		
		JSONArray segments = searchResult.getJSONArray("segments");
		//int spacesToIndentEachLevel = 2;
		//System.out.println(search_info_provided.toString(spacesToIndentEachLevel));
		//System.out.println(segments.toString(spacesToIndentEachLevel));
		
		for(int i = 0; i < resultNum; i++)
		{
			JSONObject segment = segments.optJSONObject(i);
			String source = segment.getString("source");
			String target = segment.getString("target");
			long modificationDate = segment.getLong("modificationDate");

			TmSearchResult sr = new TmSearchResult(tmInfo.getTMName(),
					tmInfo.getTMGroupName(),tmInfo.getSourceLocale(),source,
					tmInfo.getTargetLocale(),target,modificationDate);
			
			result.add(sr);
		}
	
		con.disconnect();
		return result;
	}
	
	/**
	 * Get tmList from tm_admin user using REST API, 
	 * */
	public void getTuInfo(TmServerInfo tmServerInfo, TmListInfo tmListInfo, 
			String securityTicket) throws IOException, JSONException
	{
		HttpURLConnection con = connect(tmServerInfo.getTuInfoURL(), false);
		
		JSONObject InfoSent = new JSONObject();
		InfoSent.put("securityTicket", securityTicket);
		
		OutputStream os = con.getOutputStream();
		os.write(InfoSent.toString().getBytes());
		os.flush();
		
		if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
			instance.LogMessage(this.currTime + " " + tmServerInfo.getTmUserName() + " Failed in getTuInfo: "
					+ "HTTP error code : " + con.getResponseCode());
			//throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
		}
		
		String getTuInfoResponse = getResponse(con);
		
		// Get TU info of each TM and store them into TmListInfo object.
		JSONObject tuInfoJSON = new JSONObject(getTuInfoResponse).getJSONObject("tuInfo");
		tmListInfo.setNumberOfPrivateTus(tuInfoJSON.getInt("numberOfPrivateTus"));
		tmListInfo.setLastTuModificationTimeStamp(tuInfoJSON.getLong("lastTuModificationTimestamp"));
		tmListInfo.setTotalTus(tuInfoJSON.getInt("numberOfPublicTus"));
		
		con.disconnect();
	}
	
	/**
	 * A function to call ExportTM REST API to export TM from TM Server
	 * */
	public void exportTm(TmServerInfo tmServerInfo, TmExportOption tmExportOption, 
			String securityTicket) throws IOException, JSONException
	{
		System.setProperty("jsse.enableSNIExtension", "false");
		HttpURLConnection con = connect(tmServerInfo.getExportURL(), true);
		
		JSONObject exportInfoSent = new JSONObject();
		exportInfoSent.put("exportType", tmExportOption.getExportType());
		exportInfoSent.put("includePrivateTus", tmExportOption.getIsIncludePrivateTusToString());
		exportInfoSent.put("includeAttributes", tmExportOption.getIsIncludeAttributesToString());
		exportInfoSent.put("securityTicket", securityTicket);
		
		OutputStream os = con.getOutputStream();
		os.write(exportInfoSent.toString().getBytes());
		os.flush();
		
		if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
			instance.LogMessage(this.currTime + " " + tmServerInfo.getTmUserName() + " Failed in exportTm in TmServer.java: "
					+ "HTTP error code : " + con.getResponseCode());
			// throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
		}
		// opens input stream from the HTTP connection
		InputStream inputStream = con.getInputStream();
		
		// TODO: cannot remain inputstream after get filename
		// String tmxFileName = getFileName(inputStream) + ".zip";
		
		// Construct ExportTMX Name with TMName + TMPath
		StringBuilder sb = new StringBuilder();
		sb.append(tmExportOption.getTmName() + "_");
		String tmPath = tmExportOption.getPath().replace("/", "_");
		sb.append(tmPath + ".zip");
		String tmxFileName = sb.toString();
		
		// Save the file to FTP:
		FtpUtils ftpUtilsInstance = FtpUtils.getFtpUtilInstance();
		ftpUtilsInstance.exportFileToFtp(inputStream, tmxFileName, instance.getDate());
		
		con.disconnect();
	}

	/**
	 * A function to get fileName from inputstream
	 * @throws IOException 
	 * */
	private String getFileName(InputStream inputStream) throws IOException
	{
		String tmxFileName = null;
		//get the zip file content
		ZipInputStream zis = 
				new ZipInputStream(inputStream);
		//get the zipped file list entry
		ZipEntry ze = zis.getNextEntry();
		while(ze != null){
			tmxFileName = ze.getName();
			ze = zis.getNextEntry();
		}
		zis.closeEntry();
		zis.close();
		
		System.out.println("Export TMX File: " + tmxFileName);
		return tmxFileName;
	}
	
	
}
