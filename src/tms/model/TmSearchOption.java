package tms.model;

import java.util.HashSet;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TmSearchOption {

	private String SEARCH_STRING;
	private String SEARCH_MAX_RESULT;
	private String SEARCH_LOCATION;
	private boolean SEARCH_CASE_SENTITIVE;
	private boolean SEARCH_MATCH_WHOLE_WORD;
	private Set<String> SEARCH_TM_LIST;
	
	public TmSearchOption(String searchString, String searchMaxResult, 
			String SearchLocation, boolean isCaseSentitive, 
			boolean isMatchWholeWord)
	{
		this.SEARCH_STRING = searchString;
		this.SEARCH_MAX_RESULT = searchMaxResult;
		this.SEARCH_LOCATION = SearchLocation;
		this.SEARCH_CASE_SENTITIVE = isCaseSentitive;
		this.SEARCH_MATCH_WHOLE_WORD = isMatchWholeWord;
	}
	
	public TmSearchOption(JSONObject data) throws JSONException
	{
		this.SEARCH_STRING = data.getString("concordance_search_string").trim();
		this.SEARCH_MAX_RESULT = "50";
		
		if(data.has("concordance_max_result"))
		{
			String temp = data.getString("concordance_max_result");
			if(temp != null && temp.length() > 0)
			{
				this.SEARCH_MAX_RESULT = temp;
			}
		}
		
		this.SEARCH_MAX_RESULT = data.getString("concordance_max_result");
		this.SEARCH_LOCATION = data.getString("concordance_search_location");
		this.SEARCH_CASE_SENTITIVE = data.has("concordance_case_sensitive");
		this.SEARCH_MATCH_WHOLE_WORD =  data.has("concordance_match_whole_word");
		
		// Get TM List if user select any.
		String tmListStr = data.getString("concordance_search_TM_List");
		JSONArray tmListJsonArray = new JSONArray(tmListStr);
		this.SEARCH_TM_LIST = new HashSet<String>();
		
		for(int i = 0; i < tmListJsonArray.length(); i++)
		{
			this.SEARCH_TM_LIST.add(tmListJsonArray.getString(i));
		}
		
	}
	
	public String getSearchString()
	{
		return this.SEARCH_STRING;
	}
	
	public String getSearchMaxResult()
	{
		return this.SEARCH_MAX_RESULT;
	}
	
	public String getSearchLocation()
	{
		return this.SEARCH_LOCATION;
	}
	
	public boolean getSearchCaseSentitive()
	{
		return this.SEARCH_CASE_SENTITIVE;
	}
	
	public boolean getSearchMatchWholeWord()
	{
		return this.SEARCH_MATCH_WHOLE_WORD;
	}
	
	public Set<String> getSearchTmSearch()
	{
		return this.SEARCH_TM_LIST;
	}
}
