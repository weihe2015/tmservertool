package tms.model;

public class TmServerInfo {

	private String URL;
	private String TMUSERNAME;
	private String TMPASSWORD;
	private String LOGOUT_URL;
	private String CONCORDANCE_URL;
	private String GETTUINFO_URL;
	private String EXPORT_URL;
	
	public TmServerInfo(){}
	
	public void setURL(String url)
	{
		this.URL = url + "/service/v2/login";
		this.LOGOUT_URL = this.URL.replace("login", "logout");
		this.CONCORDANCE_URL = this.URL.replace("login", "concordance");
		this.GETTUINFO_URL = this.URL.replace("login", "getTuInfo");
		this.EXPORT_URL = this.URL.replace("login", "export");
	}
	
	public String getURL()
	{
		return this.URL;
	}
	
	public String getLogoutURL()
	{
		return this.LOGOUT_URL;
	}
	
	public String getConcordanceURL()
	{
		return this.CONCORDANCE_URL;
	}
	
	public String getTuInfoURL()
	{
		return this.GETTUINFO_URL;
	}
	
	public String getExportURL()
	{
		return this.EXPORT_URL;
	}
	
	public void setTmUserName(String tmUserName)
	{
		this.TMUSERNAME = tmUserName;
	}
	
	public String getTmUserName()
	{
		return this.TMUSERNAME;
	}
	
	public void setTmPassword(String tmPassword)
	{
		this.TMPASSWORD = tmPassword;
	}
	
	public String getTmPassword()
	{
		return this.TMPASSWORD;
	}
	
}
