package tms.model;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TmSearchResult extends TmBasicInfo{

	private String SOURCE;
	private String TARGET;
	private long MODIFICATION_DATE;
	
	public TmSearchResult(String tmName, String tmGroupName, 
			String sourceLocale, String source, 
			String targetLocale, String target, long modificationDate)
	{
		super(tmName,tmGroupName,sourceLocale,targetLocale);
		this.SOURCE = source;
		this.TARGET = target;
		this.MODIFICATION_DATE = modificationDate;
	}
	
	public String getTMName()
	{
		return super.getTMName();
	}
	
	public String getTMGroupName()
	{
		return super.getTMGroupName();
	}
	
	public String getSourceLocale()
	{
		return super.getSourceLocale();
	}
	
	public String getSource()
	{
		return this.SOURCE;
	}
	
	public String getTargetLocale()
	{
		return super.getTargetLocale();
	}
	
	public String getTarget()
	{
		return this.TARGET;
	}
	
	public long getModificationDate()
	{
		return this.MODIFICATION_DATE;
	}
	
	public String getModificationDateAsString()
	{
		return String.valueOf(this.MODIFICATION_DATE);
	}
	
	public String getModificationDateInNewFormat()
	{
	    String date= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
	            .format(new Date(this.MODIFICATION_DATE));
	    return date;
	}
	
	public String getSourceLocaleDisplayName()
	{
		return super.getSourceLocaleDisplayName();
	}
	
	public String getTargetLocaleDisplayName()
	{
		return super.getTargetLocaleDisplayName();
	}
	
}
