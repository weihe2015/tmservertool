package tms.model;

import org.json.JSONException;
import org.json.JSONObject;

public class TmExportOption {

	private String TMNAME;
	private String PATH;
	private String EXPORTTYPE;
	private boolean ISINCLUDEPRIVATETUS;
	private boolean ISINCLUDEATTRIBUTES;
	
	public TmExportOption(String tmName, String path, String exportType, boolean isIncludePrivateTus, boolean isIncludeAttributes)
	{
		this.TMNAME = tmName;
		this.PATH = path;
		this.EXPORTTYPE = exportType;
		this.ISINCLUDEPRIVATETUS = isIncludePrivateTus;
		this.ISINCLUDEATTRIBUTES = isIncludeAttributes;
	}
	
	public TmExportOption(JSONObject data) throws JSONException
	{
		this.TMNAME = data.getString("tmName");
		this.PATH = data.getString("tmPath");
		this.EXPORTTYPE = data.getString("exportType");
		this.ISINCLUDEATTRIBUTES = false;
		
		if(data.has("includeAttributes"))
		{
			this.ISINCLUDEATTRIBUTES = true;
		}
		
		this.ISINCLUDEPRIVATETUS = false;
		if(data.has("includePrivateTus"))
		{
			this.ISINCLUDEATTRIBUTES = true;
		}
	}
	
	public String getTmName()
	{
		return this.TMNAME;
	}
	
	public String getPath()
	{
		return this.PATH;
	}
	
	public String getExportType()
	{
		return this.EXPORTTYPE;
	}
	
	public boolean getIsIncludePrivateTus()
	{
		return this.ISINCLUDEPRIVATETUS;
	}
	
	public String getIsIncludePrivateTusToString()
	{
		return String.valueOf(this.ISINCLUDEPRIVATETUS);
	}
	
	public boolean getIsIncludeAttributes()
	{
		return this.ISINCLUDEATTRIBUTES;
	}
	
	public String getIsIncludeAttributesToString()
	{
		return String.valueOf(this.ISINCLUDEATTRIBUTES);
	}
}
