package tms.model;

public class TmLocale {

	private String sourceLocale;
	private String targetLocale;
	
	public TmLocale(String sourceLocale, String targetLocale)
	{
		this.sourceLocale = sourceLocale;
		this.targetLocale = targetLocale;
	}
	
	public String getSourceLocale()
	{
		return this.sourceLocale;
	}
	
	public String getTargetLocale()
	{
		return this.targetLocale;
	}
	
}
