package tms.model;

import org.json.JSONException;
import org.json.JSONObject;

public class TmUnit {

	private String oldTmName;
	private String newTmName;
	private Integer tmID;
	
	public TmUnit(String oldTmName, String newTmName, Integer tmID)
	{
		this.oldTmName = oldTmName;
		this.newTmName = newTmName;
		this.tmID = tmID;
	}
	
	public TmUnit(JSONObject dataJson) throws JSONException
	{
		this.oldTmName = dataJson.getString("oldTmName").trim();
		this.newTmName = dataJson.getString("newTmName").trim();
		this.tmID = Integer.valueOf(dataJson.getString("tmId").trim());
	}
	
	public String getOldTmName()
	{
		return this.oldTmName;
	}
	
	public String getNewTmName()
	{
		return this.newTmName;
	}
	
	public Integer getTmID()
	{
		return this.tmID;
	}
	
}