package tms.model;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TmListInfo extends TmBasicInfo{

	private Integer TMID;
	private String PATH;
	private Integer NUMOFPRIVATETUS;
	private Integer TOTALTUS;
	private Long LASTTUMODIFICATIONTIME;
	
	public TmListInfo(Integer TMID, String tmName, String tmGroupName, 
			String sourceLocale, String targetLocale, String path) {
		super(tmName, tmGroupName, sourceLocale, targetLocale);
		this.TMID = TMID;
		this.PATH = path;
	}
	
	public Integer getTMID()
	{
		return this.TMID;
	}
	
	public String getPath()
	{
		return this.PATH;
	}

	public void setNumberOfPrivateTus(Integer numberOfPrivateTus)
	{
		this.NUMOFPRIVATETUS = numberOfPrivateTus;
	}
	
	public Integer getNumberOfPrivateTus()
	{
		return this.NUMOFPRIVATETUS;
	}
	
	public void setTotalTus(Integer totalTus)
	{
		this.TOTALTUS = totalTus;
	}
	
	public Integer getTotalTus()
	{
		return this.TOTALTUS;
	}
	
	public void setLastTuModificationTimeStamp(Long lastTuModificationTimeStamp)
	{
		this.LASTTUMODIFICATIONTIME = lastTuModificationTimeStamp;
	}
	
	public Long getLastTuModificationTimeStamp()
	{
		return this.LASTTUMODIFICATIONTIME;
	}
	
	public String getLastTuModificationTimeStampInNewFormat()
	{
	    String date= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
	            .format(new Date(this.LASTTUMODIFICATIONTIME));
	    return date;
	}
	
	public String getTmInfoStringForExport()
	{
		StringBuilder sb = new StringBuilder();
		
		sb.append(super.getTMName() + "#");
		sb.append(super.getSourceLocaleDisplayName() + "#");
		sb.append(super.getTargetLocaleDisplayName() + "#");
		sb.append(super.getTMGroupName() + "#");
		sb.append(this.PATH + "#");
		sb.append(this.TOTALTUS + "#");
		sb.append(this.NUMOFPRIVATETUS + "#");
		sb.append(getLastTuModificationTimeStampInNewFormat());
		
		return sb.toString();
	}
}
