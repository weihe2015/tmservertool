package tms.model;

import org.gs4tr.foundation.locale.Locale;

public class TmBasicInfo {

	private String TM_NAME;
	private String TM_GROUP_NAME;
	private String SOURCE_LOCALE;
	private String TARGET_LOCALE;
	
	public TmBasicInfo(String tmName, String tmGroupName, 
			String sourceLocale, String targetLocale)
	{
		this.TM_NAME = tmName;
		this.TM_GROUP_NAME = tmGroupName;
		this.SOURCE_LOCALE = sourceLocale;
		this.TARGET_LOCALE = targetLocale;
	}
	
	public String getTMName()
	{
		return this.TM_NAME;
	}
	
	public String getTMGroupName()
	{
		return this.TM_GROUP_NAME;
	}
	
	public String getSourceLocale()
	{
		return this.SOURCE_LOCALE;
	}
	
	public String getTargetLocale()
	{
		return this.TARGET_LOCALE;
	}
	
	public String getSourceLocaleDisplayName()
	{
		Locale srcLocale = Locale.makeLocale(this.SOURCE_LOCALE);
		return srcLocale.getDisplayName();
	}
	
	public String getTargetLocaleDisplayName()
	{
		Locale tgtLocale = Locale.makeLocale(this.TARGET_LOCALE);
		return tgtLocale.getDisplayName();
	}
	
}
