package tms.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletContext;

import com.mysql.jdbc.PreparedStatement;

import tms.model.TmListInfo;
import tms.model.TmServerInfo;
import tms.model.TmUnit;

public class HelperUtils {
	
	private static final String LOGINCONFIG = "/WEB-INF/config/login.properties";
	private static String WEBINFPATH;
	private static String DATE;
	private static String OS = System.getProperty("os.name").toLowerCase();
	
	private static String DBNAME;
	private static String DBUSER;
	private static String DBPASSWORD;
	
	private TmServerInfo TMSERVER_INFO;
	
	// TM Info result set
	private List<TmListInfo> TM_LIST_INFO_LIST;
	
	private void setTMServerInfo(TmServerInfo tmServerInfo)
	{
		TMSERVER_INFO = tmServerInfo;
	}
	
	public TmServerInfo getTMServerInfo()
	{
		return this.TMSERVER_INFO;
	}
	
	private void setTmListInfoList(List<TmListInfo> tmListInfoList)
	{
		this.TM_LIST_INFO_LIST = tmListInfoList;
	}
	
	public List<TmListInfo> getTmListInfoList()
	{
		return this.TM_LIST_INFO_LIST;
	}
	
	private HelperUtils()
	{
		
	}
	
	private static HelperUtils instance;
	
	public static HelperUtils getInstance()
	{
		if(instance == null)
		{
			instance = new HelperUtils();
		}
		return instance;
	}
	
	public com.mysql.jdbc.Connection connect() throws SQLException, ClassNotFoundException {
		// TODO Auto-generated method stub
       
		Class.forName("com.mysql.jdbc.Driver");
		
		if(DBNAME != null && DBUSER != null && DBPASSWORD != null)
		{
			String url = "jdbc:mysql://localhost:3306/"+DBNAME+"?useSSL=false";
	        return (com.mysql.jdbc.Connection) DriverManager.getConnection(url, DBUSER, DBPASSWORD);
		}
		else
		{
			return null;
		}
		
	}
	
	public com.mysql.jdbc.Connection loginConnect() throws SQLException, ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		
		if(DBNAME != null && DBUSER != null && DBPASSWORD != null)
		{
			String url = "jdbc:mysql://localhost:3306/TMServerTool?useSSL=false";
	        return (com.mysql.jdbc.Connection) DriverManager.getConnection(url, DBUSER, DBPASSWORD);
		}
		else
		{
			return null;
		}
		
	}
	
	public List<TmListInfo> getTMList(String username, 
			com.mysql.jdbc.Connection conn) throws SQLException
	{
		List<TmListInfo> result = new ArrayList<TmListInfo>();
		PreparedStatement stmt = null;
		
		try
		{
			conn.setAutoCommit(false);
			
			String SQL = "select TM_ID, NAME, TM_GROUP_NAME, SOURCE_LOCALE, TARGET_LOCALE, "
					+ "concat(PROJECT_SHORT_CODE,\"/\",SHORT_CODE) PATH from "
					+ "TM_TM m left outer join (select SHORT_CODE sc, NAME TM_GROUP_NAME from TM_PROJECT p "
					+ "left outer join TM_USER_PROFILE u on p.ORGANIZATION_ID=u.ORGANIZATION_ID "
					+ "where u.USERNAME=?) s on m.PROJECT_SHORT_CODE=s.sc where TM_GROUP_NAME IS NOT NULL";
			
			stmt = (PreparedStatement) conn.prepareStatement(SQL);
			stmt.setString(1,username);
			
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next())
			{
				Integer tmId = rs.getInt(1);
				String tmName = rs.getString(2);
				String tmGroupName = rs.getString(3);
				String sourceLocale = rs.getString(4);
				String targetLocale = rs.getString(5);
				String path = rs.getString(6);
				TmListInfo info = new TmListInfo(tmId,tmName,tmGroupName,sourceLocale,targetLocale,path);
				result.add(info);
			}
			
			setTmListInfoList(result);
		}
		finally
		{
			stmt.close();
			conn.close();
		}
		
		return result;
	}
	
	public List<TmListInfo> getAllTMList(com.mysql.jdbc.Connection conn) throws SQLException
	{
		List<TmListInfo> result = new ArrayList<TmListInfo>();
		PreparedStatement stmt = null;
		
		try
		{
			conn.setAutoCommit(false);
			
			String SQLQuery = "select TM_ID, NAME, TM_GROUP_NAME, SOURCE_LOCALE, TARGET_LOCALE, "
					+ "concat(PROJECT_SHORT_CODE,\"/\",SHORT_CODE) PATH from TM_TM m "
					+ "left outer join (select SHORT_CODE sc, NAME TM_GROUP_NAME from TM_PROJECT) p "
					+ "on m.PROJECT_SHORT_CODE=p.sc where TM_GROUP_NAME IS NOT NULL";
			
			stmt = (PreparedStatement) conn.prepareStatement(SQLQuery);
			
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next())
			{
				Integer tmId = rs.getInt(1);
				String tmName = rs.getString(2);
				String tmGroupName = rs.getString(3);
				String sourceLocale = rs.getString(4);
				String targetLocale = rs.getString(5);
				String path = rs.getString(6);
				TmListInfo info = new TmListInfo(tmId,tmName,tmGroupName,sourceLocale,targetLocale,path);
				result.add(info);
			}
			
		}
		finally
		{
			stmt.close();
			conn.close();
		}
		
		return result;
	}
	
	public boolean validateUsername(String username,
			com.mysql.jdbc.Connection conn) throws SQLException
	{
		PreparedStatement stmt = null;
		try
		{
			conn.setAutoCommit(false);
			
			String SQL = "select * from USERINFO where USER_NAME=?";
			stmt = (PreparedStatement) conn.prepareStatement(SQL);
			stmt.setString(1,username);
			
			ResultSet rs = stmt.executeQuery();
			return rs.next();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			stmt.close();
			conn.close();
		}
		return false;
	}
	
	public boolean validateLogin(String username, String password, 
			com.mysql.jdbc.Connection conn) throws SQLException
	{
		PreparedStatement stmt = null;
		
		try
		{
			conn.setAutoCommit(false);
			
			String SQL = "select * from USERINFO where USER_NAME=? and USER_PASSWORD=?";
			stmt = (PreparedStatement) conn.prepareStatement(SQL);
			stmt.setString(1,username);
			stmt.setString(2, password);
			
			ResultSet rs = stmt.executeQuery();
			return rs.next();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			stmt.close();
			conn.close();
		}
		return false;
	}
	
	public void renameTm(TmUnit unit, com.mysql.jdbc.Connection conn) throws SQLException
	{
		PreparedStatement stmt = null;
		String newTmName = unit.getNewTmName();
		Integer tmID = unit.getTmID();
		
		try
		{
			conn.setAutoCommit(false);
			
			String SQL = "UPDATE TM_TM SET NAME = ? WHERE TM_ID = ?;";
			
			stmt = (PreparedStatement) conn.prepareStatement(SQL);
			stmt.setString(1, newTmName);
			stmt.setInt(2, tmID);
			
			stmt.executeUpdate();
			conn.commit();
		}
		catch(SQLException e)
		{
			LogMessage("renameTm: " + e.getLocalizedMessage());
		}
		finally
		{
			stmt.close();
			conn.close();
		}
	}
	
	public InputStream readLoginConfig(ServletContext ctx) throws RuntimeException, IOException
	{
		InputStream input = ctx.getResourceAsStream(LOGINCONFIG);
		return input;
	}
	
	public TmServerInfo setInfo(InputStream input) throws IOException
	{
		if(getTMServerInfo() == null)
		{
			TmServerInfo info = new TmServerInfo();
			Properties properties = new Properties();
			properties.load(input);

			if(properties.containsKey("url"))
			{
				info.setURL(properties.getProperty("url"));
			}
			if(properties.containsKey("tmUsername"))
			{
				info.setTmUserName(properties.getProperty("tmUsername"));
			}
			if(properties.containsKey("tmPassword"))
			{
				info.setTmPassword(properties.getProperty("tmPassword"));
			}
			if(properties.containsKey("dbName"))
			{
				DBNAME = properties.getProperty("dbName");
			}
			if(properties.containsKey("dbUsername"))
			{
				DBUSER = properties.getProperty("dbUsername");
			}
			if(properties.containsKey("dbPassword"))
			{
				DBPASSWORD = properties.getProperty("dbPassword");
			}
			
			// Set FTP info into an object
			FtpUtils ftpUtils = FtpUtils.getFtpUtilInstance();
			if(properties.containsKey("ftpUrl"))
			{
				ftpUtils.setFtpURL(properties.getProperty("ftpUrl"));
			}
			if(properties.containsKey("ftpUsername"))
			{
				ftpUtils.setUsername(properties.getProperty("ftpUsername"));
			}
			if(properties.containsKey("ftpPassword"))
			{
				ftpUtils.setPassword(properties.getProperty("ftpPassword"));
			}
			
			setTMServerInfo(info);
		}

		return getTMServerInfo();
	}
	
	public void getErrorFolder(ServletContext ctx)
	{
		if(DATE == null)
		{
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			String date_str = dateFormat.format(date);
			setDate(date_str);
		}
		
		if(WEBINFPATH == null)
		{
			String webInfPath = ctx.getRealPath("/WEB-INF");
			setWebInfPath(webInfPath);
		}
		
	}
	
	private void setWebInfPath(String webInfPath)
	{
		WEBINFPATH = webInfPath;
	}
	
	private void setDate(String date)
	{
		DATE = date;
	}
	
	public String getDate()
	{
		return DATE;
	}
	
	public void LogMessage(String message){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		String time = dateFormat.format(date);
		
		String logFileName = "";
		if(OS.indexOf("win") > 0)
		{
			logFileName = WEBINFPATH + "\\log\\error_"+DATE+".log";
			File folder = new File(WEBINFPATH + "\\log");
			if(!folder.exists())
				folder.mkdirs();
		}
		else
		{
			logFileName = WEBINFPATH + "/log/error_"+DATE+".log";
			File folder = new File(WEBINFPATH + "/log");
			if(!folder.exists())
				folder.mkdirs();
		}
	    File logFile = new File(logFileName);
	    boolean overwrite = logFile.exists();

	    try(FileWriter fw = new FileWriter(logFileName, overwrite);
	        BufferedWriter bw = new BufferedWriter(fw);
	        PrintWriter out = new PrintWriter(bw)){
	        out.println(time+ " " + message);

	    } catch (IOException e) {
	        e.printStackTrace();
	    }

	}
	
	public void LogRenameMessage(String message)
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		String time = dateFormat.format(date);
		
		String logFileName = "";
		if(OS.indexOf("win") > 0)
		{
			logFileName = WEBINFPATH + "\\log\\renameTM_"+DATE+".log";
			File folder = new File(WEBINFPATH + "\\log");
			if(!folder.exists())
				folder.mkdirs();
		}
		else
		{
			logFileName = WEBINFPATH + "/log/renameTM_"+DATE+".log";
			File folder = new File(WEBINFPATH + "/log");
			if(!folder.exists())
				folder.mkdirs();
		}
	    File logFile = new File(logFileName);
	    boolean overwrite = logFile.exists();

	    try(FileWriter fw = new FileWriter(logFileName, overwrite);
	        BufferedWriter bw = new BufferedWriter(fw);
	        PrintWriter out = new PrintWriter(bw)){
	        out.println(time+ " " + message);

	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}
}
