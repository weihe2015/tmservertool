package tms.utils;

import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

public class FtpUtils {

	private static String FTPURL;
	private static String USERNAME;
	private static String PASSWORD;
	private String currTime;
	private HelperUtils instance;
	private final int FTP_PORT = 21;
	
	public FtpUtils()
	{
		setTime();
		HelperUtils instance = HelperUtils.getInstance();
		setHelperUtils(instance);
	}
	
	public void setFtpURL(String ftpUrl)
	{
		FTPURL = ftpUrl;
	}
	
	public void setUsername(String username)
	{
		USERNAME = username;
	}
	
	public void setPassword(String password)
	{
		PASSWORD = password;
	}
	
	private void setTime()
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date currdate = new Date();
		this.currTime = dateFormat.format(currdate);;
	}
	
	private void setHelperUtils(HelperUtils instance)
	{
		this.instance = instance;
	}
	
	public static FtpUtils FtpUtilInstance;
	
	public static FtpUtils getFtpUtilInstance()
	{
		if(FtpUtilInstance == null)
		{
			FtpUtilInstance = new FtpUtils();
		}
		return FtpUtilInstance;
	}
	
	private void ftpConnect(FTPClient ftpClient) throws SocketException, IOException
	{
		ftpClient.connect(FTPURL, FTP_PORT);
		logServerReply(ftpClient, "connect");
	}
	
	private void ftpDisconnect(FTPClient ftpClient) throws IOException
	{
		ftpClient.disconnect();
	}
	
	private void ftpLogin(FTPClient ftpClient) throws IOException
	{
		ftpClient.login(USERNAME, PASSWORD);
		logServerReply(ftpClient, "login");
	}
	
	private void ftpLogout(FTPClient ftpClient) throws IOException
	{
		ftpClient.logout();
		logServerReply(ftpClient, "logout");
	}
	
	public void exportFileToFtp(InputStream inputStream, String remoteFileName, 
			String folderName) 
			throws SocketException, IOException
	{
		FTPClient ftpClient = new FTPClient();
		ftpConnect(ftpClient);
		ftpLogin(ftpClient);
		
		if(ftpClient.isConnected())
		{
			boolean answer = ftpClient.sendNoOp();
			if(answer)
			{
				// Define FTP setting
				ftpClient.enterLocalPassiveMode();
				ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
				
				// Make a new directory if there is not an existing one.
				if(!checkDirecotryExists(ftpClient, folderName))
				{
					ftpClient.makeDirectory(folderName);
				}
				// Change working directory to this folder
                ftpClient.changeWorkingDirectory(folderName);
				
				// Upload files to FTP:
                String currTimeforExport = this.currTime;
                
                ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors()+1);
                executor.execute(new Runnable() {
                	public void run() {
                		try
                		{
            				boolean done = ftpClient.storeFile(remoteFileName, inputStream);
            				inputStream.close();
            				if(done)
            				{
            					instance.LogMessage(currTimeforExport + " FtpClient has uploaded file: " 
            							+ remoteFileName + " to FTP succesfully");
            				}
            				else
            				{
            					instance.LogMessage(currTimeforExport + " FtpClient has FAILED to upload file: " 
            							+ remoteFileName);
            				}
                		}
                		catch(IOException e)
                		{
                			instance.LogMessage(currTimeforExport + " FtpClient has FAILED to upload file: " 
            							+ remoteFileName + " with following IOException: " + e.getMessage());
                		}

                	}
                });
                executor.shutdown();
                // Wait until all threads are finish
                while (!executor.isTerminated()) {

                }
                // End of ExecutorService
			}
		}
		else
		{
			instance.LogMessage(this.currTime + " FtpClient is not connected. Please check settings." );
		}
		
		ftpLogout(ftpClient);
		ftpDisconnect(ftpClient);
	}
	
	private void logServerReply(FTPClient ftpClient, String step)
	{
		String[] replies = ftpClient.getReplyStrings();
        if (replies != null && replies.length > 0) {
            for (String aReply : replies) {
            	instance.LogMessage(this.currTime + " FTP Step: " 
            			+ step +" Server replies: " + aReply);
            }
        }
	}
	
	private boolean checkDirecotryExists(FTPClient ftpClient, String dirPath) throws IOException
	{
		ftpClient.changeWorkingDirectory(dirPath);
		int returnCode = ftpClient.getReplyCode();
		if(returnCode == 550)
		{
			return false;
		}
		return true;
	}
	
}
