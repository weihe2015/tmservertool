package tms.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import tms.api.TmServer;
import tms.model.TmExportOption;
import tms.model.TmServerInfo;
import tms.utils.HelperUtils;

/**
 * Servlet implementation class ExportTmServlet
 */
@WebServlet("/ExportTmServlet")
public class ExportTmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ExportTmServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date currdate = new Date();
		String currTime = dateFormat.format(currdate);
		
		response.setContentType("text/html; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		
		PrintWriter out = response.getWriter();
		
		HelperUtils instance = HelperUtils.getInstance();
		
		String data = request.getParameter("data").trim();
		ServletContext context = request.getServletContext();
		InputStream input = instance.readLoginConfig(context);
		TmServerInfo tmServerInfo = instance.setInfo(input);
		try
		{
			JSONObject dataJson = new JSONObject(data);
			TmExportOption tmExportOption = new TmExportOption(dataJson);
			
			TmServer server = new TmServer();
			String securityTicket = server.login(tmServerInfo, tmExportOption.getPath());
			if(securityTicket.length() == 0)
			{
				instance.LogMessage(currTime + " ExportTm Servlet: "  
						+ tmExportOption.getPath() + " has empty security Ticket");
			}
			else
			{
				server.exportTm(tmServerInfo, tmExportOption, securityTicket);
			}
			out.println("Success");
		}
		catch(JSONException e)
		{
			instance.LogMessage(currTime + " ExportTm Servlet JSONException: " + e.getLocalizedMessage());
			out.println("Fail");
		}
		
	}

}
