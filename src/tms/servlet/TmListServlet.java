package tms.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.mysql.jdbc.Connection;

import tms.api.TmServer;
import tms.model.TmListInfo;
import tms.model.TmServerInfo;
import tms.utils.HelperUtils;

/**
 * Servlet implementation class TMS_TMList
 */
@WebServlet("/TMListSerlvet")
public class TmListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TmListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		String currTime = dateFormat.format(date);
		
		response.setContentType("text/html; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		
		PrintWriter out = response.getWriter();
		
		HelperUtils instance = HelperUtils.getInstance();
		
		ServletContext context = request.getServletContext();
		InputStream input = instance.readLoginConfig(context);
		instance.getErrorFolder(context);
		
		if(input == null)
		{
			instance.LogMessage(currTime + " GetTMListServletException: No file is found");
		}
		else
		{
			TmServerInfo tmServerInfo = instance.setInfo(input);
			String username = tmServerInfo.getTmUserName();
			
			JSONObject dataJson  = new JSONObject();
			
			ConcurrentMap<String, String> currMap = new ConcurrentHashMap<String, String>();
			
			try
			{
				Connection conn = instance.connect();
				
				if(conn == null)
				{
					instance.LogMessage(currTime + " connection to database fail, please check the login.properties file.");
					return;
				}
				
				List<TmListInfo> tmListsMap = instance.getTMList(username, conn);
				
				TmServer server = new TmServer();
				
				ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors()+1);
				
				for(int i = 0; i < tmListsMap.size(); i++)
				{
					TmListInfo tmInfo = tmListsMap.get(i);

					executor.execute(new Runnable() {
		                public void run() {
							String tmName = tmInfo.getTMName();
							String tmlocation = tmInfo.getPath();
		                    try
		                    {
		    					if(tmlocation != null)
		    					{
		    						String securityTicket = server.login(tmServerInfo, tmlocation);
		    						if(securityTicket.length() > 0)
		    						{
		    							server.getTuInfo(tmServerInfo, tmInfo, securityTicket);
		    							currMap.put(tmInfo.getTmInfoStringForExport(),tmName);
		    							server.logout(tmServerInfo, securityTicket);
		    						}
		    					}
		                    }
		                    catch (Exception e)
		                    {
		                    	instance.LogMessage(currTime + " " + tmlocation + " TM List Serlvet Exception: " + e.getLocalizedMessage());
		                    }
		                }
		            });
				}
		        executor.shutdown();
		        // Wait until all threads are finish
		        while (!executor.isTerminated()) {
		 
		        }
		        
				for(String key : currMap.keySet())
				{
					String value = currMap.get(key);
					dataJson.put(key, value);
				}
								
				out.println(dataJson.toString());
			}
			catch(Exception e)
			{
				instance.LogMessage(currTime + " GetTMListServletException: " + e.getLocalizedMessage());
			}
			
		}
		
	}

}
