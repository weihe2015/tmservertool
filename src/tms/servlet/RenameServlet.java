package tms.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mysql.jdbc.Connection;

import tms.model.TmUnit;
import tms.utils.HelperUtils;


/**
 * Servlet implementation class RenameServlet
 */
@WebServlet("/RenameServlet")
public class RenameServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RenameServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date currdate = new Date();
		String currTime = dateFormat.format(currdate);
		
		response.setContentType("text/html; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		
		HelperUtils instance = HelperUtils.getInstance();
		
		String data = request.getParameter("data").trim();
		
		try
		{
			JSONArray dataArray = new JSONArray(data);
			
			for(int i = 0; i < dataArray.length(); i++)
			{
				JSONObject dataJson = dataArray.getJSONObject(i);
				
				TmUnit unit = new TmUnit(dataJson);
				
				Connection conn = instance.connect();
				
				if(conn == null)
				{
					instance.LogMessage(currTime + 
							" connection to database fail, please check the login.properties file.");
					return;
				}
				
				instance.renameTm(unit, conn);
				
				StringBuilder sb = new StringBuilder();
				sb.append("TM with TM_ID : " + unit.getTmID() + " has updated its name from: " + unit.getOldTmName());
				sb.append(" to: " + unit.getNewTmName());
				
				instance.LogRenameMessage(sb.toString());
			}
			
		}
		catch (JSONException e) {
			// TODO Auto-generated catch block
			instance.LogMessage(currTime + " Rename Servlet JSONException: " + e.getLocalizedMessage());
			//e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			instance.LogMessage(currTime + " Rename Servlet ClassNotFoundException: " + e.getLocalizedMessage());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			instance.LogMessage(currTime + " Rename Servlet SQLException: " + e.getLocalizedMessage());
		}
		
	}

}
