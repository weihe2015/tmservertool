package tms.servlet;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import com.mysql.jdbc.Connection;

import tms.model.TmListInfo;
import tms.utils.HelperUtils;

/**
 * Servlet implementation class TmInfoServlet
 */
@WebServlet("/TmInfoServlet")
public class TmInfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TmInfoServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		String currTime = dateFormat.format(date);
		
		response.setContentType("text/html; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		
		PrintWriter out = response.getWriter();
		
		HelperUtils instance = HelperUtils.getInstance();
		ServletContext context = request.getServletContext();
		InputStream input = instance.readLoginConfig(context);
		instance.getErrorFolder(context);
		
		if(input == null)
		{
			instance.LogMessage(currTime + " Get TmInfoServletException: No file is found");
		}
		else
		{
			JSONArray dataArray = new JSONArray();
				
			try
			{
				Connection conn = instance.connect();
				
				if(conn == null)
				{
					instance.LogMessage(currTime + " connection to database fail, please check the login.properties file.");
					return;
				}
				
				List<TmListInfo> tmInfoLists = instance.getAllTMList(conn);
				
				for(TmListInfo info : tmInfoLists)
				{
					String TMName = info.getTMName();
					
					StringBuilder sb = new StringBuilder();
					sb.append(info.getTMGroupName());
					sb.append("#");
					sb.append(info.getTMID());
					sb.append("#");
					sb.append(info.getPath());
					sb.append("#");
					sb.append(info.getSourceLocaleDisplayName());
					sb.append("#");
					sb.append(info.getTargetLocaleDisplayName());
					
					JSONObject tmJson  = new JSONObject();
					tmJson.put("label", TMName);
					tmJson.put("value", sb.toString());
					dataArray.put(tmJson);
				}
				out.println(dataArray.toString());
				
			}
			catch(Exception e)
			{
				instance.LogMessage(currTime + " GetTmInfoServletException: " + e.getLocalizedMessage());
			}
			
		}
		
	}
	

}