package tms.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.mysql.jdbc.Connection;

import tms.api.TmServer;
import tms.model.TmListInfo;
import tms.model.TmSearchOption;
import tms.model.TmSearchResult;
import tms.model.TmServerInfo;
import tms.utils.HelperUtils;

/**
 * Servlet implementation class ConcordanceSearch_Servlet
 */
@WebServlet("/ConcordanceServlet")
public class ConcordanceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConcordanceServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	protected String highLightedText(String searchString, String sourceLine, boolean caseSensitive)
	{
	    Pattern regex = null;
	    if(caseSensitive)
	    {
	    	regex = Pattern.compile(
	 		        searchString);
	    }
	    else
	    {
	    	regex = Pattern.compile(
	    	        searchString, Pattern.CASE_INSENSITIVE);
	    }
	    int start = 0;
	    Matcher matcher = regex.matcher(sourceLine);

	    StringBuilder sb = new StringBuilder();

	    while (matcher.find(start))
	    {
	    	if (matcher.start() > start)
	    	{
	    		sb.append(sourceLine.substring(start, matcher.start()));
	    	}
	    	String content = matcher.group();
	    	content = "<span style=\"background-color:yellow;\">" + content + "</span>";
	    	sb.append(content);
	    	start = matcher.end();
	    }

	    if (start < sourceLine.length())
	    {
	    	sb.append(sourceLine.substring(start));
	    }
	    return sb.toString();
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date currdate = new Date();
		String currTime = dateFormat.format(currdate);
		
		response.setContentType("text/html; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		
		PrintWriter out = response.getWriter();
		
		String data = request.getParameter("data").trim();
		
		HelperUtils instance = HelperUtils.getInstance();
		
		try {
			// Construct a JSONObject and pass to TmSearchOption constructor.
			JSONObject dataJson  = new JSONObject(data);
			
			TmSearchOption searchOption = new TmSearchOption(dataJson);
			
			ServletContext context = request.getServletContext();
			InputStream input = instance.readLoginConfig(context);
			instance.getErrorFolder(context);
			
			if(input == null)
			{
				instance.LogMessage(currTime + "No properties file is found");
			}
			else
			{
				TmServerInfo tmServerInfo = instance.setInfo(input);
				String username = tmServerInfo.getTmUserName();
				TmServer server = new TmServer();

				List<TmSearchResult> searchResult = 
						Collections.synchronizedList(new ArrayList<TmSearchResult>());
				try
				{
					Connection conn = instance.connect();
					
					if(conn == null)
					{
						instance.LogMessage(currTime + 
								" connection to database fail, please check the login.properties file.");
						return;
					}
					
					ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors()+1);
					
					List<TmListInfo> tmListsMap;
					if(instance.getTmListInfoList() == null)
					{
						tmListsMap = instance.getTMList(username, conn);
					}
					else
					{
						tmListsMap = instance.getTmListInfoList();
					}
					
					conn.close();
					
					for(int i = 0; i < tmListsMap.size(); i++)
					{
						TmListInfo tmInfo = tmListsMap.get(i);
						
						String tmName = tmInfo.getTMName();						
						if(!searchOption.getSearchTmSearch().isEmpty() && 
								!searchOption.getSearchTmSearch().contains(tmName))
						{
							continue;
						}
						
						executor.execute(new Runnable() {
			                public void run() {
								String tmPath = tmInfo.getPath();
			                    try
			                    {
									if(tmPath != null)
									{
										String securityTicket = server.login(tmServerInfo, tmPath);
										if(securityTicket.length() == 0)
										{
											instance.LogMessage(currTime + " Concordance Servlet: "  
													+ tmPath + " has empty security Ticket");
										}
										else
										{
											List<TmSearchResult> result = 
													server.concordance(tmServerInfo,securityTicket,searchOption,tmInfo);
											
											if(result != null)
											{
												searchResult.addAll(result);
											}
											
											server.logout(tmServerInfo, securityTicket);
										}
									}
			                    }
			                    catch (Exception e)
			                    {
			                    	instance.LogMessage(currTime + " Concordance Servlet: " 
			                    			+ tmPath + " Exception: " + e.getLocalizedMessage());
			                    }
			                }
			            });
						
					}
					
			        executor.shutdown();
			        // Wait until all threads are finish
			        while (!executor.isTerminated()) {
			 
			        }
					
				}
				catch(Exception e)
				{
					instance.LogMessage(currTime + " Exception occurs: " + e.getLocalizedMessage());
				}
				
				printResultTableHeader(out);
				
				if(searchResult.size() == 0)
				{
					instance.LogMessage(currTime + " No result is found for string: " + searchOption.getSearchString());
					// No result is found
				}
				else
				{
					for(int i = 0; i < searchResult.size(); i++)
					{
						TmSearchResult rs = searchResult.get(i);

						String tmName = rs.getTMName();
						String tmGroupName = rs.getTMGroupName();

						String modificationDate = rs.getModificationDateInNewFormat();

						String sourceLocaleDisplayName = rs.getSourceLocaleDisplayName();
						String source = rs.getSource();
						String targetLocaleDisplayName = rs.getTargetLocaleDisplayName();
						String target = rs.getTarget();

						if(searchOption.getSearchLocation().equalsIgnoreCase("SOURCE"))
						{
							source = highLightedTexts(searchOption, source);
						}
						else if(searchOption.getSearchLocation().equalsIgnoreCase("TARGET"))
						{
							target = highLightedTexts(searchOption, target);
						}
						else if(searchOption.getSearchLocation().equalsIgnoreCase("SOURCE_AND_TARGET"))
						{					
							source = highLightedTexts(searchOption, source);
							target = highLightedTexts(searchOption, target);
						}

						out.println("<tr><td>"+tmName+"</td><td>"+sourceLocaleDisplayName+"</td>"
								+ "<td>"+source+"</td><td>"+targetLocaleDisplayName+"</td><td>"+target+"</td>"
								+ "<td class='extraInfo'>"+tmGroupName+"</td>"
								+ "<td class='extraInfo'>"+modificationDate+"</td></tr>");
					}
	
					out.println("</tbody></table>");
					
					printResultAllTable(out,searchResult);
				}
			}			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			instance.LogMessage(currTime + " JSONException: " + e.getLocalizedMessage());
			//e.printStackTrace();
		}
		
	}
	
	private void printResultTableHeader(PrintWriter out)
	{
		out.println("<hr>");
		out.println("<div class=\"row\">");
		out.println("<div class=\"col-xs-6\">");
		out.println("</div>");
		out.println("<div class=\"col-xs-3\">");
		out.println("</div>");
		out.println("<div class=\"col-xs-3\">");
		out.println("<button class=\"btn btn-primary btn-lg\" data-toggle=\"modal\" "
				+ "data-target=\"#exportDataOptions\" style=\"float: right;\">"
				+ "Export data into Excel</button>");
		out.println("</div>");
		out.println("</div><br />");
		
		out.println("<table id=\"result-table\" class=\"table table-striped table-bordered display\" cellspacing=\"0\" width=\"100%\">"
				+ "<thead><tr><th>TM Name</th><th>Source Language</th>"
				+ "<th>Source</th><th>Target Language</th><th>Target</th><th class='extraInfo'>TM Group</th>"
				+ "<th class='extraInfo'>Modification Date</th></tr></thead>");
		out.println("<tfoot><tr><th>TM Name</th><th>Source Language</th>"
				+ "<th>Source</th><th>Target Language</th><th>Target</th><th class='extraInfo'>TM Group</th>"
				+ "<th class='extraInfo'>Modification Date</th></tr></tfoot><tbody>");
	}
	
	private void printResultAllTable(PrintWriter out, List<TmSearchResult> searchResult)
	{
		// print out an invisiblle table for download all data
		out.println("<table id=\"result-all\" style=\"display: none\" cellspacing=\"0\" width=\"100%\">"
				+ "<thead><tr><th>TM Name</th><th>Source Language</th>"
				+ "<th>Source</th><th>Target Language</th><th>Target</th><th>TM Group</th>"
				+ "<th>Modication Date</th></tr></thead>");
		out.println("<tfoot><tr><th>TM Name</th><th>Source Language</th>"
				+ "<th>Source</th><th>Target Language</th><th>Target</th><th>TM Group</th>"
				+ "<th>Modication Date</th></tr></tfoot>");
		out.println("<tbody>");
		
		for(int i = 0; i < searchResult.size(); i++)
		{
			TmSearchResult rs = searchResult.get(i);
			String TM_Name = rs.getTMName();
			String source = rs.getSource();
			String target = rs.getTarget();			
			String tmGroupName = rs.getTMGroupName();
			String modificationDate = rs.getModificationDateInNewFormat();
		    
			out.println("<tr><td>"+TM_Name+"</td><td>"+rs.getSourceLocaleDisplayName()+"</td>"
					+ "<td>"+source+"</td><td>"+rs.getTargetLocaleDisplayName()+"</td><td>"+target+"</td>"
					+ "<td>"+tmGroupName+"</td><td>"+modificationDate+"</td></tr>");
		}

		out.println("</tbody></table>");
		out.println("<div class=\"row\">");
		out.println("<div class=\"col-xs-6\">");
		out.println("</div></div>");
	}
	
	private String highLightedTexts(TmSearchOption searchOption, String content)
	{
		String[] splited = searchOption.getSearchString().split("\\s+");
		for(String split : splited)
		{
			content = highLightedText(split, content, searchOption.getSearchCaseSentitive());
		}
		return content;
	}

}
