package tms.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.jdbc.Connection;

import tms.utils.HelperUtils;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date currdate = new Date();
		String currTime = dateFormat.format(currdate);
		
		response.setContentType("text/html; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		
		PrintWriter out = response.getWriter();
		
		String username = request.getParameter("username").trim();
		String password = request.getParameter("password").trim();
		
		HelperUtils instance = HelperUtils.getInstance();
		
		ServletContext context = request.getServletContext();
		InputStream input = instance.readLoginConfig(context);
		instance.getErrorFolder(context);
		instance.setInfo(input);
		
		try
		{
			Connection conn = instance.loginConnect();
			
			if(conn == null)
			{
				instance.LogMessage(currTime + " connection to database fail");
				out.println("false, connection is null");
				return;
			}
			boolean checkUsername = instance.validateUsername(username, conn);
			
			if(!checkUsername)
			{
				out.println("false, username does not exists");
			}
			else
			{
				if(conn.isClosed())
				{
					conn = instance.loginConnect();
				}
				
				boolean loginStatus = instance.validateLogin(username, password, conn);
				if(loginStatus)
				{
					out.println("true");
				}
				else
				{
					out.println("false, password does not match");
				}
			}
			
			conn.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			instance.LogMessage(e.getLocalizedMessage());
		}

	}

}
